module Queries
  class FilterAndOrder
    attr_reader :customers

    def initialize(customers:)
      @customers = customers
    end

    def call
      less_than_100km_away = filter_command.call(customers: customers)
      ordered_by_user_id = order_command.call(customers: less_than_100km_away)
    end

    private

    def filter_command
      @filter_command ||= Queries::FilterCustomersByMaximumDistance.new
    end

    def order_command
      @order_command ||= Queries::OrderCustomersByUserId.new
    end
  end
end
