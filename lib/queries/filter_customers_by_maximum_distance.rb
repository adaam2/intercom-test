module Queries
  class FilterCustomersByMaximumDistance
    attr_reader :maximum_distance

    def initialize(maximum_distance: Constants::MAX_DISTANCE_IN_KM)
      @maximum_distance = maximum_distance
    end

    def call(customers:)
      customers
        .select { |customer| distance_present?(customer) }
        .select { |customer| within_maximum_radius?(customer) }
    end

    private

    def distance_present?(customer)
      customer.distance.present?
    end

    def within_maximum_radius?(customer)
      customer.distance <= maximum_distance
    end
  end
end
