module Queries
  class OrderCustomersByUserId
    def call(customers:)
      customers
        .sort_by(&:user_id)
    end
  end
end
