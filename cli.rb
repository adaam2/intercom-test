require 'optparse'
require_relative 'autoload.rb'
require 'colorize'

class CLI
  attr_accessor :options

  def initialize
    @options = {}
  end

  def run
    ARGV << '-h' if ARGV.empty?

    OptionParser.new do |parser|
      parser.banner = 'Usage: ruby cli.rb -p path/to/file'

      parser.on('-p', '--path PATH', 'Path to customers.txt') do |path|
        options[:path] = path
        begin
          lines = Parsing::TextFileLineSplitter.new(input_file_path).call
        rescue => e
          puts e.message
          exit
        end

        json_array = lines.map { |line| json_parser.call(line) }
        customers = json_array
                      .map { |json| customer_parser.call(json) }
                      .compact

        invalid_customers = customers.select(&:invalid?)

        customers = customers - invalid_customers

        elligible_customers = Queries::FilterAndOrder.new(customers: customers).call

        log "#{double_newline}The customers located within #{max_distance} kilometre radius are:#{double_newline}"
        log(elligible_customers.map { |c| c.to_s(true).green })

        rejected_customers = (customers - elligible_customers)

        unless rejected_customers.empty?
          log "#{double_newline}The customers that were excluded due to being over the maximum distance were:#{double_newline}"

          log(rejected_customers.map { |c| c.to_s(true).light_blue })
        end

        unless invalid_customers.empty?
          log "#{double_newline}The customers excluded due to invalid input data were:#{double_newline}"

          log(invalid_customers.map { |c| "#{c.to_s} #{c.errors.full_messages}".red })
        end

        log(double_newline)
      end

      parser.on_tail('-h', '--help', 'Help guide') do
        log parser
        exit
      end
    end.parse!
  end

  private

  def max_distance
    @max_distance ||= Constants::MAX_DISTANCE_IN_KM
  end

  def json_parser
    @json_parser ||= Parsing::JSONParser.new
  end

  def customer_parser
    @customer_parser ||= Parsing::JSONToCustomer.new
  end

  def input_file_path
    raise 'No path found' if options[:path].blank?
    File.join(__dir__, options[:path])
  end

  def log(message)
    puts message
  end

  def double_newline
    "\n\n"
  end
end

CLI.new.run
