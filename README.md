# TLDR

Run:

```
ruby cli.rb --path=customers.txt
```

# Running the application

First, `cd` to the root of the project on the command line. You will need to install Bundler and the gems specified in the `Gemfile` using:

```
gem install bundler
bundle install
```

I have provided a simple CLI utility which takes a few arguments. Arguments are processed using the Ruby `optparse` library. You can see the various arguments that are available by running:

```
➜  intercom-test git:(master) ✗ ruby cli.rb --help

Usage: ruby cli.rb -p path/to/file
    -p, --path PATH                  Path to customers.txt
    -h, --help                       Help guide
```

To run with the default settings:

```
ruby cli.rb --path=customers.txt
```

An overview of the acceptable CLI args is shown below:

Argument | Description | Example
--- | --- | ---
--path | Customers.txt file path. By default, `customers.txt` is stored in the root of the project, so simply passing `customers.txt` for the path value is acceptable | `ruby cli.rb --path=customers.txt`
--help | Shows the help guide | `ruby cli.rb --help`

# Running the test suite

To run all of the tests:

```
rspec
```

# Handling of invalid data

Each JSON object in the `customers.txt` input data file is deserialized into an instance of the `Customer` model. Blank rows are ignored.

The `Customer` class includes methods from `ActiveModel::Model` class which provides a lot of the base functionality within Rails AR models, including data (presence) validations for model attributes and other lifecycle methods.

Any incomplete input data is run through the `Customer` model validations and any rows with invalid data in them are excluded from the final output list.

Currently the validations that are performed are:

- *Name*: checks presence
- *Latitude*: checks presence, numericality in valid latitude range of `-90` to `90`
- *Longitude*: checks presence, numericality in valid longitude range of `-180` to `180`
- *User ID*: checks presence, numericality (needs to be an integer)


# Sample output format

The expected output will look like this:

```
The customers located within 100 kilometres are:

Ian Kehoe - ID 4 (roughly 11 kilometres away)
Nora Dempsey - ID 5 (roughly 23 kilometres away)
Theresa Enright - ID 6 (roughly 24 kilometres away)
Eoin Ahearn - ID 8 (roughly 84 kilometres away)
Richard Finnegan - ID 11 (roughly 38 kilometres away)
Christina McArdle - ID 12 (roughly 42 kilometres away)
Olive Ahearn - ID 13 (roughly 62 kilometres away)
Michael Ahearn - ID 15 (roughly 44 kilometres away)
Patricia Cahill - ID 17 (roughly 96 kilometres away)
Eoin Gallagher - ID 23 (roughly 83 kilometres away)
Rose Enright - ID 24 (roughly 89 kilometres away)
Stephen McArdle - ID 26 (roughly 99 kilometres away)
Oliver Ahearn - ID 29 (roughly 72 kilometres away)
Nick Enright - ID 30 (roughly 83 kilometres away)
Alan Behan - ID 31 (roughly 44 kilometres away)
Lisa Ahearn - ID 39 (roughly 38 kilometres away)


The customers that were excluded due to being over the maximum distance were:

Alice Cahill - ID 1 (roughly 313 kilometres away)
Ian McArdle - ID 2 (roughly 324 kilometres away)
Jack Enright - ID 3 (roughly 189 kilometres away)
Charlie Halligan - ID 28 (roughly 109 kilometres away)
Frank Kehoe - ID 7 (roughly 211 kilometres away)
Enid Gallagher - ID 27 (roughly 152 kilometres away)
Jack Dempsey - ID 9 (roughly 133 kilometres away)
Georgina Gallagher - ID 10 (roughly 131 kilometres away)
Helen Cahill - ID 14 (roughly 278 kilometres away)
Ian Larkin - ID 16 (roughly 168 kilometres away)
Bob Larkin - ID 18 (roughly 166 kilometres away)
Enid Cahill - ID 19 (roughly 224 kilometres away)
Enid Enright - ID 20 (roughly 238 kilometres away)
David Ahearn - ID 21 (roughly 275 kilometres away)
Charlie McArdle - ID 22 (roughly 180 kilometres away)
David Behan - ID 25 (roughly 161 kilometres away)
```
