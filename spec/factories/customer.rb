require 'factory_bot'

FactoryBot.define do
  factory :customer do
    skip_create

    name { Faker::Name.name }
    latitude { Faker::Address.latitude }
    longitude { Faker::Address.longitude }
    user_id { rand(4) }

    trait :with_distance do
      distance { rand(3) }
    end

    trait :with_no_lat_lng do
      latitude { nil }
      longitude { nil }
    end
  end
end
