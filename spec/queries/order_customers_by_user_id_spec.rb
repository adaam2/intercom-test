RSpec.describe Queries::OrderCustomersByUserId do
  describe '#call' do
    let(:customer_one) { create :customer, user_id: 1 }
    let(:customer_two) { create :customer, user_id: 3 }
    let(:customer_three) { create :customer, user_id: 2 }

    let(:customers) { [customer_one, customer_two, customer_three] }

    it 'orders the array by user id ascending' do
      expect(subject.call(customers: customers))
        .to eq(
          [customer_one, customer_three, customer_two]
        )
    end
  end
end
