RSpec.describe Queries::FilterCustomersByMaximumDistance do
  let(:maximum_distance) { 100 }

  subject { described_class.new(maximum_distance: maximum_distance) }

  let(:inside_radius_one) { create :customer, distance: 10 }
  let(:inside_radius_two) { create :customer, distance: 99 }
  let(:outside_radius) { create :customer, distance: 199 }

  let(:customers) { [inside_radius_one, inside_radius_two, outside_radius] }

  describe '#call' do
    it 'only returns the customers inside of the 100km radius' do
      expect(subject.call(customers: customers))
        .to match_array(
          [inside_radius_one, inside_radius_two]
        )
    end

    context 'When one of the customers doesnt have a distance set' do
      let(:no_distance_set) { create :customer, distance: nil }
      let(:customers) { [inside_radius_one, no_distance_set] }

      it 'does not include the customer with no distance calculated' do
        expect(subject.call(customers: customers))
          .to eq [inside_radius_one]
      end
    end
  end
end
