RSpec.describe Queries::FilterAndOrder do
  let(:customer_one) { create :customer }
  let(:customer_two) { create :customer }
  let(:customer_three) { create :customer }

  let(:customers) { [customer_one, customer_two, customer_three] }
  let(:filtered_customers) { [customer_one, customer_two] }
  let(:ordered_customers) { [customer_two, customer_one] }

  subject { described_class.new(customers: customers) }

  let(:filter_double) { instance_double Queries::FilterCustomersByMaximumDistance, call: filtered_customers }
  let(:order_double) { instance_double Queries::OrderCustomersByUserId, call: ordered_customers }

  before do
    allow(Queries::FilterCustomersByMaximumDistance)
      .to receive(:new)
      .and_return(filter_double)
    allow(Queries::OrderCustomersByUserId)
      .to receive(:new)
      .and_return(order_double)
  end

  describe '#call' do
    it 'calls the filtering by distance command with the correct customers array' do
      subject.call

      expect(filter_double)
        .to have_received(:call)
        .with(customers: customers)
        .once
    end

    it 'calls the ordering command with the result of the filtering by distance command' do
      subject.call

      expect(order_double)
        .to have_received(:call)
        .with(customers: filtered_customers)
        .once
    end

    it 'returns the ordered_customers array returned from the ordering command to the caller' do
      expect(subject.call)
        .to eq ordered_customers
    end
  end
end
