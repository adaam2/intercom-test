RSpec.describe Customer, type: :model do
  describe 'validations' do
    shared_examples 'Valid' do
      it 'is valid' do
        expect(customer.valid?).to eq true
      end
    end

    shared_examples 'Invalid' do
      it 'is invalid' do
        expect(customer.valid?).to eq false
      end
    end

    describe '#user_id presence' do
      context 'When present' do
        let(:customer) { create :customer, user_id: 1234 }

        it_behaves_like 'Valid'
      end

      context 'When not present' do
        let(:customer) { create :customer, user_id: nil }

        it_behaves_like 'Invalid'
      end
    end

    describe '#user_id numericality' do
      context 'When user_id is an integer' do
        let(:customer) { create :customer, user_id: 1234 }

        it_behaves_like 'Valid'
      end

      context 'When user_id is a string' do
        let(:customer) { create :customer, user_id: 'hello' }

        it_behaves_like 'Invalid'
      end

      context 'When the user id is a float' do
        let(:customer) { create :customer, user_id: 12334.455 }

        it_behaves_like 'Invalid'
      end
    end

    describe '#latitude presence' do
      context 'When present' do
        let(:customer) { create :customer, latitude: 45 }

        it_behaves_like 'Valid'
      end

      context 'When not present' do
        let(:customer) { create :customer, latitude: nil }

        it_behaves_like 'Invalid'
      end
    end

    describe '#latitude numericality' do
      context 'When a valid latitude' do
        let(:customer) { create :customer, latitude: -90 }

        it_behaves_like 'Valid'
      end

      context 'When latitude is outside of acceptable range' do
        let(:customer) { create :customer, latitude: -100 }

        it_behaves_like 'Invalid'
      end
    end

    describe '#longitude presence' do
      context 'When present' do
        let(:customer) { create :customer, longitude: 89 }

        it_behaves_like 'Valid'
      end

      context 'When not present' do
        let(:customer) { create :customer, longitude: nil }

        it_behaves_like 'Invalid'
      end
    end

    describe '#longitude numericality' do
      context 'When a valid longitude' do
        let(:customer) { create :customer, longitude: -170 }

        it_behaves_like 'Valid'
      end

      context 'When longitude is outside of acceptable range' do
        let(:customer) { create :customer, longitude: -200 }

        it_behaves_like 'Invalid'
      end
    end

    describe '#name presence' do
      context 'When present' do
        let(:customer) { create :customer, name: 'Adam Bull' }

        it_behaves_like 'Valid'
      end

      context 'When not present' do
        let(:customer) { create :customer, name: nil }

        it_behaves_like 'Invalid'
      end
    end
  end

  describe '#to_s' do
    let(:customer) { create :customer, :with_distance, name: 'Adam Bull', user_id: 1234, distance: 1 }

    context 'Basic implementation' do
      it 'returns the correctly formatted string' do
        expect(customer.to_s)
          .to eq 'Adam Bull - ID 1234'
      end
    end

    context 'When include_distance parameter is true' do
      it 'returns the correctly formatted string with the distance calculation appended to the end' do
        expect(customer.to_s(true))
          .to eq 'Adam Bull - ID 1234 (roughly 1 kilometres away)'
      end
    end
  end

  describe 'Class methods' do
    describe '#from_json' do
      let(:json_hash) do
        {
          name: 'Adam Bull',
          latitude: 299292,
          longitude: 493390
        }
      end

      it 'constructs the Customer object from the JSON hash' do
        customer = subject.class.from_json(json_hash)

        expect(customer.name).to eq 'Adam Bull'
        expect(customer.latitude).to eq 299292
        expect(customer.longitude).to eq 493390
      end
    end
  end
end
