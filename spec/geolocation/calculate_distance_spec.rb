RSpec.describe Geolocation::CalculateDistance do
  describe '#call' do
    let(:intercom_lat) { 53.339428 }
    let(:intercom_lng) { -6.257664 }

    let(:test_lat) { 53.2451022 }
    let(:test_lng) { -6.238335 }

    let(:args) do
      {
        lat1: test_lat,
        lon1: test_lng,
        lat2: intercom_lat,
        lon2: intercom_lng
      }
    end

    it 'returns the expected distance value in km' do
      expect(
        subject.call(args)
      ).to be_within(0.1).of(10.5)
    end

    context 'When any/all of the input values are nil' do
      let(:test_lat) { nil }

      it 'raises an exception' do
        expect {
          subject.call(args)
        }.to raise_error('Please provide valid coordinates')
      end
    end
  end
end
