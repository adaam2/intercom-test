RSpec.describe Parsing::JSONParser do
  describe '#call' do
    let(:json_string) { '{ "latitude": "40.83524014629543", "longitude": "128.14978192048284" }' }

    it 'returns the latitude and longitude values as floats' do
      expect(subject.call(json_string).symbolize_keys)
        .to match(
          latitude: 40.83524014629543,
          longitude: 128.14978192048284
        )
    end

    context 'When the input string is invalid JSON' do
      it 'raises a JSON::ParserError exception' do
        expect {
          subject.call('sfkjd')
        }.to raise_error(JSON::ParserError)
      end
    end

    context 'When latitude value is missing' do
      let(:json_string) { '{ "longitude": "128.14978192048284" }' }

      it 'does not raise an exception' do
        expect {
          subject.call(json_string)
        }.not_to raise_error
      end

      it 'sets the unprovided lat or lng values to nil' do
        expect(subject.call(json_string)['latitude'])
          .to be_nil
      end
    end

    context 'When latitude value is missing' do
      let(:json_string) { '{ "latitude": "128.14978192048284" }' }

      it 'does not raise an exception' do
        expect {
          subject.call(json_string)
        }.not_to raise_error
      end

      it 'sets the unprovided lat or lng values to nil' do
        expect(subject.call(json_string)['longitude'])
          .to be_nil
      end
    end
  end
end
