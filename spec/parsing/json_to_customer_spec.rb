RSpec.describe Parsing::JSONToCustomer do
  let(:name) { 'Adam Bull' }
  let(:latitude) { Faker::Address.latitude }
  let(:longitude) { Faker::Address.longitude }

  let(:json_object) do
    {
      name: name,
      latitude: latitude,
      longitude: longitude
    }.to_json
  end

  let(:customer) { create :customer, name: name, latitude: latitude, longitude: longitude }

  let(:calculate_distance_double) { instance_double Geolocation::CalculateDistance, call: 40 }

  before do
    allow(Customer)
      .to receive(:from_json)
      .with(json_object)
      .and_return(customer)

    allow(Geolocation::CalculateDistance)
      .to receive(:new)
      .and_return(calculate_distance_double)
  end

  it 'calls the Customer.to_json method' do
    subject.call(json_object)

    expect(Customer)
      .to have_received(:from_json)
      .with(json_object)
      .once
  end

  it 'calls the calculate_distance command' do
    subject.call(json_object)

    expect(calculate_distance_double)
      .to have_received(:call)
      .with(
        lat1: latitude,
        lon1: longitude,
        lat2: Constants::INTERCOM_COORDINATES[0],
        lon2: Constants::INTERCOM_COORDINATES[1]
      )
      .once
  end

  it 'returns the newly created customer object' do
    expect(subject.call(json_object))
      .to eq customer
  end

  context 'When the customer is missing latitude or longitude values' do
    before do
      allow(calculate_distance_double)
        .to receive(:call)
        .and_raise('Please provide valid coordinates')
    end

    let(:customer) { create :customer, :with_no_lat_lng }

    it 'returns the customer without the distance set so that invalid records can still be piped to the STDOUT' do
      expect(subject.call(json_object).distance)
        .to be_nil
    end

    it 'does not raise an error' do
      expect {
        subject.call(json_object)
      }.not_to raise_error
    end
  end
end
